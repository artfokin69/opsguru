#pragma glslify: ease = require(glsl-easings/quartic-in-out)
#pragma glslify: perlin2d = require(glsl-noise/simplex/2d)
#pragma glslify: perlin3d = require(glsl-noise/simplex/3d)

varying vec2 vUv;
varying vec3 vPosition;
varying vec3 vNormal;
varying vec3 vColor;
varying vec3 vAltColor;

uniform float hoverProgress;
uniform sampler2D tAudioData;
uniform float fqAvg;
uniform float time;
uniform bool animatePosition;

attribute vec3 color;
attribute vec3 altColor;
attribute float size;



void main() {
  vNormal = normal;
  vPosition = position;
  vColor = color;
  vAltColor = altColor;
  vUv = uv;
  float audioNoise = perlin3d(position.xyz + vec3(time / 10.));
  vec3 newpos = position;
  if(fqAvg > 40. && animatePosition){
    newpos.y += 2. * perlin3d(newpos * fqAvg);
  }
  vec4 mvPosition = modelViewMatrix * vec4( newpos, 1. );
  gl_PointSize = (5. + size * 300.) * ( 1. / - mvPosition.z );
  gl_Position = projectionMatrix * mvPosition;
}